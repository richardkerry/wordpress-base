FROM ubuntu:latest
MAINTAINER LevelUP Solutions "dev@levelup.solutions"

ENV APACHE_CONFDIR /etc/apache2
ENV APACHE_ENVVARS $APACHE_CONFDIR/envvars

# Install apache, PHP, and supplimentary programs. openssh-server, curl, and lynx-cur are for debugging the container.
RUN apt-get update && apt-get -y upgrade && DEBIAN_FRONTEND=noninteractive apt-get -y install \
    apache2 \
    libapache2-modsecurity \
	php7.0 php7.0-mysql libapache2-mod-php7.0 curl lynx-cur \
	php7.0-json php7.0-curl php7.0-gd php7.0-xml php7.0-bcmath php7.0-mcrypt \
	php7.0-ldap php7.0-bz2 php7.0-soap php7.0-zip \
	php7.0-fileinfo php7.0-gettext php7.0-mbstring php7.0-exif php7.0-opcache

# Apache Modules
RUN a2enmod rewrite
RUN a2enmod php7.0
RUN a2enmod headers
RUN a2dismod autoindex -f
RUN a2dismod ssl
RUN a2dismod status
RUN a2enmod mpm_prefork
RUN a2dismod mpm_worker
RUN a2dismod mpm_event

RUN set -ex \
	&& . "$APACHE_ENVVARS" \
	&& ln -sfT /dev/stderr "$APACHE_LOG_DIR/error.log" \
	&& ln -sfT /dev/stdout "$APACHE_LOG_DIR/access.log"

# Setup Apache Tuning
RUN sed -i "s/Timeout 300/Timeout 100/" /etc/apache2/apache2.conf
RUN sed -i "s/MaxKeepAliveRequests 100/MaxKeepAliveRequests 1000/" /etc/apache2/apache2.conf

# Setup basic Apache Security Settings
RUN sed -i "s/ServerTokens OS/ServerTokens Prod/" /etc/apache2/conf-enabled/security.conf
RUN sed -i "s/ServerSignature On/ServerSignature Off/" /etc/apache2/conf-enabled/security.conf

# Update the PHP.ini file, enable <? ?> tags and quieten logging.
RUN sed -i "s/short_open_tag = Off/short_open_tag = On/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/error_reporting = .*$/error_reporting = E_ERROR | E_WARNING | E_PARSE/" /etc/php/7.0/apache2/php.ini

# Enable PHP opcache
RUN sed -i "s/;opcache.enable=0/opcache.enable=1/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/;opcache.max_accelerated_files=2000/opcache.max_accelerated_files=7963/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/;opcache.memory_consumption=64/opcache.memory_consumption=192/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/;opcache.interned_strings_buffer=4/opcache.interned_strings_buffer=16/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/;opcache.fast_shutdown=0/opcache.fast_shutdown=1/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/;opcache.validate_timestamps=1/opcache.validate_timestamps=0/" /etc/php/7.0/apache2/php.ini
RUN sed -i "s/;opcache.revalidate_freq=2/opcache.revalidate_freq=0/" /etc/php/7.0/apache2/php.ini

# Rename the ModSecurity Config file
RUN mv /etc/modsecurity/modsecurity.conf-recommended /etc/modsecurity/modsecurity.conf

# Update the modsecurity module to be On and Setup Base Rulesets
RUN sed -i "s/SecRuleEngine DetectionOnly/SecRuleEngine On/" /etc/modsecurity/modsecurity.conf
RUN sed -i "s/SecResponseBodyAccess On/SecResponseBodyAccess Off/" /etc/modsecurity/modsecurity.conf
RUN echo "Include /usr/share/modsecurity-crs/*.conf" > /etc/modsecurity/modsecurity.conf
RUN echo "Include /usr/share/modsecurity-crs/activated_rules/*.conf" > /etc/modsecurity/modsecurity.conf

RUN ln -s /usr/share/modsecurity-crs/base_rules/modsecurity_crs_20_protocol_violations.conf /usr/share/modsecurity-crs/activated_rules/modsecurity_crs_20_protocol_violations.conf
RUN ln -s /usr/share/modsecurity-crs/base_rules/modsecurity_crs_20_protocol_violations.data /usr/share/modsecurity-crs/activated_rules/modsecurity_crs_20_protocol_violations.data

RUN ln -s /usr/share/modsecurity-crs/base_rules/modsecurity_crs_30_http_policy.conf /usr/share/modsecurity-crs/activated_rules/modsecurity_crs_30_http_policy.conf
RUN ln -s /usr/share/modsecurity-crs/base_rules/modsecurity_crs_30_http_policy.data /usr/share/modsecurity-crs/activated_rules/modsecurity_crs_30_http_policy.data


# Manually set up the apache environment variables
ENV APACHE_RUN_USER www-data
ENV APACHE_RUN_GROUP www-data
ENV APACHE_LOG_DIR /var/log/apache2
ENV APACHE_LOCK_DIR /var/lock/apache2
ENV APACHE_PID_FILE /var/run/apache2.pid

EXPOSE 80

# Update the default apache site with the config we created.
ADD apache-config.conf /etc/apache2/sites-enabled/000-default.conf

# Add in the Web Content
ADD www /var/www/site

# Make MMR Directory Writeable
# RUN chown -R www-data:www-data /var/www/site/wp-content/mmr
# RUN chmod -R +rw /var/www/site/wp-content/mmr

# By default start up apache in the foreground, override with /bin/bash for interative.
CMD /usr/sbin/apache2ctl -D FOREGROUND
