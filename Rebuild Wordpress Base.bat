docker network disconnect WordPressNet mysql
docker network disconnect WordPressNet wordpress
docker network rm WordPressNet
docker stop mysql
docker rm mysql
docker run --name mysql -e MYSQL_ROOT_PASSWORD=webapp -e MYSQL_DATABASE=webapp -e MYSQL_USER=webapp -e MYSQL_PASSWORD=webapp -d mysql:latest 
docker build -t wordpress C:\Users\richa\Documents\Dev\wordpress-base
docker stop wordpress
docker rm wordpress
docker run --name wordpress -p 80:80 -d -e VIRTUAL_HOST=www.wordpressbase.local wordpress
docker network create WordPressNet
docker network connect WordPressNet mysql
docker network connect WordPressNet wordpress