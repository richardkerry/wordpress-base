<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'webapp');

/** MySQL database username */
define('DB_USER', 'webapp');

/** MySQL database password */
define('DB_PASSWORD', 'webapp');

/** MySQL hostname */
define('DB_HOST', 'mysql');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
 define('AUTH_KEY',         '/xN&V?D.{7MTuaJ_N_$z`S*(*!A>ff:bmeqwTcF~Ztzlf7qB~BGgfx6Gc01nS{pD');
 define('SECURE_AUTH_KEY',  'Uf+8:Qg T`Pld,v<-EcEc/d3/^*)+%G[%-%OR6OWK}nV[Skit|E]g<3pXJJsm=$-');
 define('LOGGED_IN_KEY',    'F`UQ/2-)wkw2KP]QYsM~WEA@>-WFu7@Cfo}]$+_r#cc.rXP)fl4Ke]9r~O;?q|[D');
 define('NONCE_KEY',        'm-Q?xum0I+Wg2N|t)4dGl1r}quX3Jn;3cm!Tb]e1Tq)6FHnWiavxyZ8w40aQ$g6B');
 define('AUTH_SALT',        'B5GLOgJe*+9 dyU)kY/Y2.$m Y9~A+5iztjmDd+n`[8*v@vLr~i@W2nLcF~qsL<m');
 define('SECURE_AUTH_SALT', 'W3&WKL_Pv6,Pzw%ptmLV5x:22nU&s-*b#Rb$ky,~!-i_t0kBHG|O,2yZI-8/&z8c');
 define('LOGGED_IN_SALT',   'V|bj7}^|V1pcZ`mGh`&jx4>B7.CmdyT(zI~ZL5g@|,rl#3<RhnrrKFRW~l!3HDWO');
 define('NONCE_SALT',       'LN5l7B+)RP|pP=]{:x+PT8#|`8khRzcf+18Ko!H}^YaX=eyr<NNnV{VGnGe}r#[K');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
